package com.edu.pe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialYoutubeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TutorialYoutubeApplication.class, args);
    }

}
